#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>

/*****************************************************************/
// Fixture class
class MaterialPointTest : public ::testing::Test {
protected:
  void SetUp() override {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> material_points;
    n_points = 10;
    for (UInt i = 0; i < n_points; ++i) {
      MaterialPoint p;
      p.getPosition() = 1.;
      p.getTemperature() = 1.;
      p.getHeatRate() = 1.;
      material_points.push_back(p);
    }

    for (auto& p : material_points) {
      // std::cout << p << std::endl;
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }
  }

  System system;
  UInt n_points;
};

/*****************************************************************/
TEST_F(MaterialPointTest, csv) {
  CsvWriter writer("tmp_file");
  writer.compute(system);

  CsvReader reader("tmp_file");
  System s_fromfile;
  reader.compute(s_fromfile);

  EXPECT_EQ(n_points, s_fromfile.getNbParticles());

  for (UInt i = 0; i < n_points; ++i) {
    auto& part1 = static_cast<MaterialPoint&>(system.getParticle(i));
    auto& tem1 = part1.getTemperature();
    auto& part2 = static_cast<MaterialPoint&>(s_fromfile.getParticle(i));
    auto& tem2 = part2.getTemperature();
    ASSERT_NEAR(tem1, tem2, std::abs(tem1) * 1e-12);
  }
}
/*****************************************************************/

// Fixture class
class MaterialPointTests : public ::testing::Test {

protected:
  void SetUp() override {
    MaterialPoint p1, p2, p3, p4;
    p1.getPosition() = 0.;
    p2.getPosition() = 0.;
    p3.getPosition() = 0.;
    p4.getPosition() = 0.;

    p1.getPosition()[0] = -1.;
    p1.getPosition()[1] = -1.;
    p2.getPosition()[0] = 1.;
    p2.getPosition()[1] = -1.;
    p3.getPosition()[0] = 1.;
    p3.getPosition()[1] = 1.;
    p4.getPosition()[0] = -1;
    p4.getPosition()[1] = 1.;


    p1.getHeatRate() = 0.;
    p2.getHeatRate() = 0.;
    p3.getHeatRate() = 0.;
    p4.getHeatRate() = 0.;
    
    system.addParticle(std::make_shared<MaterialPoint>(p1));
    system.addParticle(std::make_shared<MaterialPoint>(p2));
    system.addParticle(std::make_shared<MaterialPoint>(p3));
    system.addParticle(std::make_shared<MaterialPoint>(p4));
    
    n_points = 4;
    temperature = std::make_shared<ComputeTemperature>(1.);
    temperature->setLengthDomain(2, 2);
  }

  System system;
  UInt n_points;

  std::shared_ptr<ComputeTemperature> temperature;
};

/*****************************************************************/
TEST_F(MaterialPointTests, homogeneous) {
  UInt n_iterations = 100;
  
  auto& p1 = static_cast<MaterialPoint&>(system.getParticle(0));
  auto& p2 = static_cast<MaterialPoint&>(system.getParticle(1));
  auto& p3 = static_cast<MaterialPoint&>(system.getParticle(2));
  auto& p4 = static_cast<MaterialPoint&>(system.getParticle(3));

  p1.getTemperature() = 0.;
  p2.getTemperature() = 1.;
  p3.getTemperature() = 1.;
  p4.getTemperature() = 0.;

  for(UInt i = 0; i < n_iterations; ++i){
    temperature->compute(system);
    std::cout << p1.getTemperature() << " " << p2.getTemperature() << " " <<
      p3.getTemperature() << " " << p4.getTemperature() << std::endl;
  }
  
  ASSERT_NEAR(p1.getTemperature(), 0.5, 1e-10);
  ASSERT_NEAR(p2.getTemperature(), 0.5, 1e-10);
  ASSERT_NEAR(p3.getTemperature(), 0.5, 1e-10);
  ASSERT_NEAR(p4.getTemperature(), 0.5, 1e-10);

}
/*****************************************************************/

// Fixture class
class MaterialPointTestHeatSource : public ::testing::Test {

protected:

  void SetUp() override {
    //Boundaries of the domain
    Real x_min = -1.;
    Real x_max = 1.;
    Real y_min = -1.;
    Real y_max = 1.;
    //Discretization of the domain
    UInt N_grid = 9;
    Real Lx = x_max - x_min;
    Real Ly = y_max - y_min;
    Real dx = Lx/(N_grid - 1);
    Real dy = Ly/(N_grid - 1);
    //For a square domain
    L = Lx;
    //Initial temperature field
    Real T = 0;
    //Time interval
    Real dt = 1.;

    for (UInt i = 0; i < N_grid; i++){
      for (UInt j = 0; j < N_grid; j++) {
	MaterialPoint par;
	par.getPosition() = 0.;
	Real x = x_min + i*dx;
	par.getPosition()[0] = x;
	par.getPosition()[1] = y_min + j * dy;
	par.getHeatRate() = pow(2.*M_PI/L,2) * sin(2.*M_PI*x/L);
	par.getTemperature() = T;
	system.addParticle(std::make_shared<MaterialPoint>(par));
      }
    }
    n_points = N_grid * N_grid;
    temperature = std::make_shared<ComputeTemperature>(dt);
  }

  System system;
  UInt n_points;
  Real L;
  
  std::shared_ptr<ComputeTemperature> temperature;
};

/*****************************************************************/

TEST_F(MaterialPointTestHeatSource, sinusoidalflux) {
  UInt n_iterations = 10;
  //ComputeTemperature temperature(dt);
  auto& p4 = static_cast<MaterialPoint&>(system.getParticle(4));

  for(UInt i = 0; i < n_iterations; ++i){
    std::cout << p4.getTemperature() << std::endl;
    temperature->compute(system);
  }

  for (UInt i = 0; i < n_points; i++){
    auto& par = static_cast<MaterialPoint&>(system.getParticle(i));
    Real x = par.getPosition()[0];
    Real temp = sin(2*M_PI*x/L);
    ASSERT_NEAR(par.getTemperature(), temp, 1e-10);
    }
}
/*****************************************************************/


// Fixture class
class MaterialPointTestHeatSource2 : public ::testing::Test {

protected:
    //Boundaries of the domain
    Real x_min = -1;
    Real x_max = 1;
    Real y_min = -1;
    Real y_max = 1;
    //Discretization of the domain
    Real N_grid = 2;
    Real Lx = x_max - x_min;
    Real Ly = y_max - y_min;
    Real dx = Lx/(N_grid - 1);
    Real dy = Ly/(N_grid - 1);
    //For a square domain
    Real L = Lx;
    //Initial temperature field
    Real T = 10;
    //Time interval
    Real dt = 0.1;

    void SetUp() override {
        for (UInt i = 0; i < N_grid; i++){
            for (UInt j = 0; j < N_grid; j++) {
                MaterialPoint par;
                par.getPosition()[0] = x_min + i * dx;
                par.getPosition()[1] = y_min + j * dy;
                if (x_min + i * dx == 0.5)
                    par.getHeatRate() = 1;
                else if (x_min + i * dx == -0.5)
                    par.getHeatRate() = -1;
                else
                    par.getHeatRate() = -1;
                //par.getTemperature() = T;
                system.addParticle(std::make_shared<MaterialPoint>(par));
            }
        }
        n_points = N_grid * N_grid;
        temperature = std::make_shared<ComputeTemperature>(dt);
    }

    System system;
    UInt n_points;

    std::shared_ptr<ComputeTemperature> temperature;
};

/*****************************************************************/

TEST_F(MaterialPointTestHeatSource2, intervalflux) {
    UInt n_iterations = 100;
    //ComputeTemperature temperature(dt);

    UInt count = 0;
    for (UInt i = 0; i < N_grid; i++) {
        for (UInt j = 0; j < N_grid; j++) {
            if (i + j > 0)
                ++count;
            auto &par = static_cast<MaterialPoint &>(system.getParticle(count));
            par.getTemperature() = T;
        }
    }

    for(UInt i = 0; i < n_iterations; ++i){
        temperature->compute(system);
        auto& par = static_cast<MaterialPoint&>(system.getParticle(0));
        std::cout << par.getTemperature() << std::endl;
    }

    for (UInt i = 0; i < n_points; i++){
        auto& par = static_cast<MaterialPoint&>(system.getParticle(i));
        Real temp;
        if (par.getPosition()[0] <= -0.5)
            temp = - par.getPosition()[0] - 1;
        else if (par.getPosition()[0] >= 0.5)
            temp = - par.getPosition()[0] + 1;
        else
            temp = - par.getPosition()[0];
        std::cout << par.getTemperature() << std::endl;
        ASSERT_NEAR(par.getTemperature(), temp, 1e-10);
    }
}

