#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

ComputeTemperature::ComputeTemperature(Real dt) : dt(dt) {}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setDeltaT(Real dt) { this->dt = dt; }

/* -------------------------------------------------------------------------- */


void ComputeTemperature::setDensity(Real rho) { this->rho = rho; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setHeatCapacityC(Real C) { this->C = C; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setHeatConductivity(Real K) { this->K = K; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setLengthDomain(Real x, Real y) {
  this->x = x;
  this->y = y;
}


/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {

  UInt N = sqrt(system.getNbParticles());

  // The frequency domain
  Matrix<std::complex<int>> freq = FFT::computeFrequencies(N);

  Matrix<complex> th(N);
  Matrix<complex> hv(N);
  for (auto&& entry : index(th)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    int k = i*N+j;
    auto& temp = std::get<2>(entry);
    auto& heat = hv(i, j);
    auto& par = static_cast<MaterialPoint&>(system.getParticle(k));
    // Create a matrix with temperatures
    temp = par.getTemperature();
    // Get heat source
    heat = par.getHeatRate();
}

/* -------------------------------------------------------------------------- */
// Transform to Fourier space
auto th_ = FFT::transform(th);
auto hv_ = FFT::transform(hv);


Matrix<complex> dth_dt_(N);
for (auto&& entry : index(dth_dt_)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& dth_ = std::get<2>(entry);
    auto& h_ = hv_(i, j);
    auto& t_ = th_(i, j);

    complex qx = 2*M_PI/x*freq(i, j).real();
    complex qy = 2*M_PI/y*freq(i, j).imag();

    // Not sure here: (Maybe divide by total time instead)
    dth_ = 1./(rho*C)*(h_ - K*t_*(qx*qx+qy*qy));
}

auto dth_dt = FFT::itransform(dth_dt_);

// Update temperature in the system
for (auto&& entry : index(dth_dt)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    int k = i*N + j;
    auto& val = std::get<2>(entry);

    auto& par = static_cast<MaterialPoint&>(system.getParticle(k));
    par.getTemperature() += dt*val.real();
}
}
