#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

  // Virtual implementation
public:
  // Constructors/Destructors
  ComputeTemperature(Real dt); 
  //! Set time step
  void setDeltaT (Real dt);
   //! Set mass density
  void setDensity(Real rho);
  //! Set specific heat capacity
  void setHeatCapacityC(Real C);
  //! Set heat conductivity
  void setHeatConductivity(Real K);
  //! Set dimension length
  void setLengthDomain(Real x, Real y);
   //! Temperature Transmission 
  void compute(System& system) override;

private:
  Real dt; 
  Real rho = 1.;
  Real C = 1.;
  Real K = 1.;
  Real x = 1.;
  Real y = 1.;

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
