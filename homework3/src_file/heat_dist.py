import numpy as np

##Function that creates the heat distribution
def heat_dist(x,y,R):
    if x**2+y**2<R:
        value = 1
    else:
        value = 0;
    return value

##Set the radius
R=50
##Boundaries of the domain
x_min = -10
x_max = 10
y_min = -10
y_max = 10
##Discretization of the domain
N_grid = 20
Lx = x_max - x_min;
Ly = y_max - y_min;
dx = Lx/(N_grid - 1);
dy = Ly/(N_grid - 1);

hv = np.zeros((N_grid,N_grid))

for i in range(0,N_grid):
    for j in range(0,N_grid):
        hv[i,j] = heat_dist(x_min + i*dx,y_min + j*dy,R)


np.savetxt("heat_distribution.csv", hv, delimiter=" ",fmt='%1.3f')