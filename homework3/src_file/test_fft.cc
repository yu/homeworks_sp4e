#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {

  UInt N = 512;
  Matrix<complex> m(N);

  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (i == 1 && j == 0)
      val = N * N / 2.0;
    else if (i == N - 1 && j == 0)
      val = N * N / 2.0;
    else
      val = 0;
  }

  Matrix<complex> res = FFT::itransform(m);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    ASSERT_NEAR(std::abs(val), std::abs(cos(k * i)), 1e-10);
  }

}

/*****************************************************************/

TEST(FFT, computeFrequencies) {
  int N = 9;
  int freq_test[] = {0, 1, 2, 3, 4, -4, -3, -2, -1};
  auto freq_fft = FFT::computeFrequencies(N);

  for (auto&& entry : index(freq_fft)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    ASSERT_NEAR(val.real(), freq_test[j], 1e-10);
    ASSERT_NEAR(val.imag(), freq_test[i], 1e-10);
  }
}

/*****************************************************************/
