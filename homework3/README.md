# Homework 3
Group Members/Authors: Yuhan and Mati Ullah

## Requirements

Minimum requirments of C++ environment are given below: 

* cmake

* g++

* FFTW

* GoogleTest
 
To install FFTW,

```
sudo apt install libfftw3-dev
```

## Usage

To compile and build the programme, following the commands below:

```
cd homework3/src_file
mkdir build
cd build
ccmake ..
make
``` 

The usage of the programme should follow the command with format below:

```
./particles [num_step] [dump_freq] [input_file] [particle_type] [timestep]
```

The arguments includes:

`num_step`: state the number of simulation steps, should be a int.

`dump_freq`: state the frequency at which to dump output, should be a int.

`input_file`: state the input csv file and the file path containing the starting state of the particles, should be a string.

`particle_type`: the type of particle for simulation, could be "planet", "ping_pong" or "material_point", should be a string.

`timestep`: the timestep of the simulation, should be a float.

For example, a possible command could be:

```
./particles 10 1 input.csv planet 0.01
```


## Answers to questions

### Exercise 1: Code discovery  
Question: Describe how the particles are organized in the various objects.

Answer: With inheritance, classes including `MaterialPoint`, `ping-pong ball` and `planet` can be taken as daughter classes of the mother class `particle`.

Managed by Singleton, the mother class `particle` and related daughter classes `MaterialPoint`, `ping-pong ball` and `planet` be created, consist `particles_factory_interface` as mother class, as well as `MaterialPointsFactory`, `PingPongBallsFactory` and `PlanetsFactory` as daughter classes.

For the additional classes please see the below summary:
* `MaterialPoint` class is a daughter class of the mother class `particle' . 
* `MaterialPointsFactory` class is used to generate material point instances and is the daughter class of 'ParticlesFacroryInterface'. 
* `Matrix`is templated strut for the computations and operations of 2D matrices. 
* `FTT` class is for the handling of Fast Fourier Transform (FFT) operations, linking with the appropriate FFTW library.

### Exercise 2: Link to FFTW

FFTW was not linked to exceutable. In this exercise the FFTW is dynanmically linked with the excecutable by modifying the CmakeList.txt file.

### Exercise 4: Solver Implementation 
Question 4.5: Explain in the README how you integrate this condition within the existing code.

Answer: The python script named "heat_dist.py," generates a heat distribution matrix and exports it to a CSV file named "heat_distribution.csv."

To customize the generated heat distribution, you'll need to set the following parameters within the script:
* Radius (R)
* Minimum/Maximum x coordinates (x_min, x_max)
* Minimum/Maximum y coordinates (y_min, y_max)
* Grid domain discretization
The values contained in the heat_distribution.csv file can be read and replace the creation of the hv matrix inside the ComputeTemperature implementation.
To apply boundary conditions, modify the compute method in ComputeTemperature.cc:
* Set the initial temperature condition.
* Update the temperature based on matrix indexes.

Question 4.6:  Describe how to launch such a stimulation which will produce dumps observable with Paraview for a grid of 512 × 512 particles.

Answer:
Following steps should adopt to launch the simulation with ParaView:
* Open ParaView and go to File > Open > ' .csv' > Open Data With CSV Reader.

* In the Properties window:
  1. Uncheck 'Have Headers.'
  2. Set 'Field Delimiter Characters' to space as ' '.
  3. Click on Apply.

* On the menu, navigate to Filters > Search, and look for 'Table to Points,' then press Enter.

* In the Properties window:
  1. Choose X, Y, and Z columns.
  2. Select 2D points.
  3. Click Apply.
  4. Under Display (Geometry Representation), change Coloring
  5. Click show/hide color legend for the legend.

## Note:
There are few things in the code that are not working as intended. Therefore, the answer of question 4.6 is given based on paraview example done in class.  
