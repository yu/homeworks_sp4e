#ifndef COMPUTE_PI_HH
#define COMPUTE_PI_HH

#include "Series.hh"
#include <cmath>

class ComputePi : public Series {
public:
    
    ComputePi();
    
    double compute(unsigned int N) override;

    double computeTerm(unsigned int k) override;

    double getAnalyticPrediction() override;

};

#endif //COMPUTE_PI_HH
