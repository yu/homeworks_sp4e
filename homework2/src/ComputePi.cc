#define _USE_MATH_DEFINES

#include <cmath>
#include "ComputePi.hh"

// Constructor for the ComputePi class
ComputePi::ComputePi() : Series() {}

// Method to compute an approximation of pi using a series
double ComputePi::compute(unsigned int N) {

    Series::compute(N); // Delegates the computation to the base class Series
    return sqrt(6.0 * this->current_value); // Calculate the square root of 6 times the current value
    
}

// Method to compute a specific term (k) in the series for pi approximation
double ComputePi::computeTerm(unsigned int k) {

    return 1.0 / (1.0 * k * k); // Calculate the kth term in the series

}

// Method to get the analytic prediction of pi using the M_PI constant
double ComputePi::getAnalyticPrediction() {
    return M_PI;
}
