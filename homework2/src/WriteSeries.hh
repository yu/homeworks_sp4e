#ifndef WRITE_SERIES_HH
#define WRITE_SERIES_HH

#include <iostream>
#include <fstream>

#include "DumperSeries.hh"

class WriteSeries: public DumperSeries{
public:
    WriteSeries(Series& series, unsigned int maxiter, char separator);
    void dump(std::ostream & os = std::cout) override;

private:
    int maxiter;
    char separator;
    void setSeparator(char separator);

};
#endif //WRITE_SERIES_HH
