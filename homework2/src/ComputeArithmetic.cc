#include "ComputeArithmetic.hh"

// Constructor for the ComputeArithmetic class
ComputeArithmetic::ComputeArithmetic() : Series() {}

// Method to compute the arithmetic series sum up to the Nth term
double ComputeArithmetic::compute(unsigned int N) {
    
    return Series::compute(N);// Delegates the computation to the base class Series

}

// Method to compute a specific term (k) in the arithmetic series
double ComputeArithmetic::computeTerm(unsigned int k) {

    return 1.*k; // Returns the kth term of the arithmetic series
    
}
double ComputeArithmetic::getAnalyticPrediction() {
    return 1. * this->current_index*(this->current_index+1)/2.0;
}
