#ifndef PRINT_SERIES_HH
#define PRINT_SERIES_HH

#include <iostream>

#include "Series.hh"
#include "DumperSeries.hh"

class PrintSeries : public DumperSeries {

public:

    PrintSeries(Series& series, unsigned int maxiter, unsigned int frequency = 1);

    void dump(std::ostream& os = std::cout) override;

private:

    unsigned int frequency;

    unsigned int maxiter;

};

#endif //PRINT_SERIES_HH