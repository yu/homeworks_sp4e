#include "WriteSeries.hh"
// Constructor for WriteSeries class
WriteSeries::WriteSeries(Series& series, unsigned int maxiter, char separator) : DumperSeries(series) {
    // Initialize the maxiter and separator member variables
    this->maxiter = maxiter;
    this->separator = separator;
}

// Function to write series data to the given output stream
void WriteSeries::dump(std::ostream & os) {
    double res, res2, conv;
    int N = this->maxiter;
    setSeparator(this->separator);

    // Iterate through the series and write data to the output stream
    for (int k = 1; k <= N; ++k) {
        res = this->series.compute(k-1);
        res2 = this->series.compute(k);
        conv = abs(this->series.getAnalyticPrediction() - res2);

        // Write data to the output stream with specific formatting
        os << std::scientific << std::setprecision(6) << std::setw(5)
           << k << this->separator << std::setw(15)
           << res  << this->separator << std::setw(15)
           << res2 - res << this->separator << std::setw(15);

        // Check for conv variable and write it if it's not a NaN
        if (!std::isnan(conv)) {
            os << std::setw(15) << conv;
        }
        os << std::endl;
    }
}

// Function to set the separator character
void WriteSeries::setSeparator(char separator) {
    this->separator = separator;
}
