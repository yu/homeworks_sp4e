////////////////////////////////////////////////////////////////////////////////
//                                 Home work 02                               //
//                          Group: Yuhan and Mati Ullah                       //
//****************************************************************************//
                             //Adding Headers//
//****************************************************************************//
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
#include <string>
#include <cmath>

#include "Series.hh"
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "DumperSeries.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"
#include "RiemannIntegral.hh"

///////////////////////////////////////////////////////////////////////////////


int main(int argc, char **argv){
 
    std::stringstream sstr;

    std::string series_type;        // initial argument 1
    unsigned int iteration_num;     // initial argument 2
    unsigned int frequency;         // initial argument 3
    unsigned int max_iteration = 1000;

    std::string function;
    double a;                       // lower limit
    double b;                       // upper limit

    std::string dumper_type;        // initial argument 4
    std::string write_format = "txt";       // initial argument 5, valid when dumper type is "write"


    // detect if the arguments input is correct
    if (argc != 6 && argc != 9) {
        std::cerr << "File: " << argv[0] << " " << "Arguments input ERROR, please check!" << std::endl;
        return EXIT_FAILURE;
    }
    else {
        std::cout << "Arguments check pass" << std::endl;

    }


    if (std::string(argv[1]) == "arithmetic" || std::string(argv[1]) == "pi") {

        //Input arguments confirm
        sstr << argv[1] << " " << argv[2] << " " << argv[3] << " " << argv[4] << " " << argv[5];
        sstr >> series_type >> dumper_type >> iteration_num >> frequency >> write_format;
    
        std::cout << "INPUT ARGUMENTS CONFIRM" << std::endl;
        std::cout << "Series type: " << argv[1] << std::endl;
        std::cout << "Output option: " << argv[2] << std::endl;
        std::cout << "Number of iteration: " << argv[3] << std::endl;
        std::cout << "Record frequency: " << argv[4] << std::endl;
        std::cout << "File format: " << argv[5] << std::endl;


        // declare series pointer
        Series *series_pt;
        if(series_type == "arithmetic"){
            series_pt = new ComputeArithmetic();
        }
        else if (series_type == "pi") {
            series_pt = new ComputePi();
        }

        // declare dumper output
        DumperSeries *dumper_pt;
        if (dumper_type == "print") {
            dumper_pt = new PrintSeries(*series_pt, max_iteration, frequency);
            std::cout << *dumper_pt;
        }
        else if (dumper_type == "write") {
            std::ofstream f("numerical_results." + write_format);
            char separator;
            if (write_format == "csv")
                separator = ',';
            else if (write_format == "txt")
                separator = ' ';
            else if (write_format == "psv")
                separator = '|';
            else
                separator = ' ';            //By default, write a .txt file

            dumper_pt = new WriteSeries(*series_pt, max_iteration, separator);
            f << *dumper_pt;
            f.close();

        }

        delete series_pt;
        delete dumper_pt;

    }


    else if (std::string(argv[1]) == "integral") {

        //Input arguments confirm
        sstr << argv[1] << " " << argv[2] << " " << argv[3] << " " << argv[4] << " " << argv[5] << " " << argv[6] << " " << argv[7] << " " << argv[8];
        sstr >> series_type >> dumper_type >> iteration_num >> frequency >> a >> b >> function >> write_format;

        std::cout << "INPUT ARGUMENTS CONFIRM" << std::endl;
        std::cout << "Series type: " << argv[1] << std::endl;
        std::cout << "Output option: " << argv[2] << std::endl;
        std::cout << "Number of iteration: " << argv[3] << std::endl;
        std::cout << "Record frequency: " << argv[4] << std::endl;
        std::cout << "Lower limit a: " << argv[5] << std::endl;
        std::cout << "Upper limit b: " << argv[6] << std::endl;
        std::cout << "Function type: " << argv[7] << std::endl;
        std::cout << "File format: " << argv[8] << std::endl;


        // declare series pointer
        Series *series_pt;
        if (function == "x3")
            series_pt = new RiemannIntegral(a, b, x3);
        else if (function == "cos")
            series_pt = new RiemannIntegral(a, b, cosx);
        else if (function == "sin")
            series_pt = new RiemannIntegral(a, b, sinx);
    


        // declare dumper output
        DumperSeries *dumper_pt;
        if (dumper_type == "print") {
            dumper_pt = new PrintSeries(*series_pt, max_iteration, frequency);
            std::cout << *dumper_pt;
        }
        else if (dumper_type == "write") {
            std::ofstream f("numerical_results." + write_format);
            char separator;
            if (write_format == "csv")
                separator = ',';
            else if (write_format == "txt")
                separator = ' ';
            else if (write_format == "psv")
                separator = '|';
            else
                separator = ' ';            //By default, write a .txt file

            dumper_pt = new WriteSeries(*series_pt, max_iteration, separator);
            f << *dumper_pt;
            f.close();

        }

        delete series_pt;
        delete dumper_pt;
    }

    return 0;

}