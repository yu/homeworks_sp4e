#ifndef DUMPER_SERIES_HH
#define DUMPER_SERIES_HH

#include <iostream>
#include <iomanip>

#include "Series.hh"


class DumperSeries {
public:
    
	DumperSeries(Series & series);
	
	virtual void dump(std::ostream & os) = 0;

	void setPrecision(unsigned int precision);

protected:

	Series & series;

	unsigned int precision;

};

inline std::ostream & operator << (std::ostream & stream, DumperSeries & _this) {

	_this.dump(stream);
	return stream;

}

#endif //DUMPER_SERIES_HH
