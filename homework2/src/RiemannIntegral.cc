//
// Created by Lenovo on 11/1/2023.
//

#include "RiemannIntegral.hh"


RiemannIntegral::RiemannIntegral(double a, double b, std::function<double(double)> f) : f(f) {
    // Constructor initializes the RiemannIntegral object with provided values.
    this->a = a; // Set the lower bound 'a'.
    this->b = b; // Set the upper bound 'b'.
    this->use_previous = false; // Initialize use_previous flag to false.
}

double RiemannIntegral::compute(unsigned int N) {
    // Compute the Riemann integral using the specified number of subintervals 'N'.
    this->dx = (this->b -this->a)/N; // Calculate the width of each subinterval.
    return Series::compute(N); // Delegate the computation to the Series class and return the result.
}

double RiemannIntegral::computeTerm(unsigned int k){
    // Calculate the k-th term of the Riemann integral.
    double xi = this->a + this->dx*k; // Calculate the x-coordinate for this term within the subinterval.
    return f(xi)*this->dx; // Compute the product of f(xi) and the subinterval width (dx).
}


double x3(double x) {
    return pow(x, 3);
}
double cosx(double x) {
    return cos(x);
}
double sinx(double x) {
    return sin(x);
}