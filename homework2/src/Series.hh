#ifndef SERIES_HH
#define SERIES_HH

#include <cmath>


class Series {
public:
    Series();
    
    virtual double compute(unsigned int N);

    virtual double computeTerm(unsigned int k) = 0;

    virtual double getAnalyticPrediction();

    void addTerm();

protected: 

    unsigned int current_index;

    double current_value;

    bool use_previous;

};

#endif //SERIES_HH
