#ifndef COMPUTE_ARITHMETIC_HH
#define COMPUTE_ARITHMETIC_HH

#include<iostream>

#include "Series.hh"

// ComputeArithmetic is derived class from Series for computing sum of natural numbers 
class ComputeArithmetic: public Series {
public:
    
    ComputeArithmetic();

    // Overrides the compute method from the base class
    double compute(unsigned int N) override;
     
    // Overrides the computeTerm method from the base class
    double computeTerm(unsigned int k) override;

    double getAnalyticPrediction() override;

};

#endif // COMPUTE_ARITHMETIC_HH
