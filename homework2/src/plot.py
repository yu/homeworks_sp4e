import matplotlib.pyplot as plt
import argparse

# Define a function to read data from a file
def read_file(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    data = []
    separator = None

    for line in lines:
        if separator is None:
            # Detect the separator based on the first line
            if ',' in line:
                separator = ','
            elif '|' in line:
                separator = '|'
            else:
                separator = ' '

        values = line.strip().split(separator)
        numeric_values = []

        for val in values:
            try:
                numeric_values.append(float(val))
            except ValueError:
                pass  # Skip non-numeric values

        if numeric_values:
            data.append(numeric_values)

    return data

# Define a function to plot the data
def plot_results(x, y, out_file=''):
    # Create a figure and axis for the plot
    fig, axe = plt.subplots()

    # Plot the data as red dots with a label
    axe.plot(x, y, marker='o', color='red', label='Numerical')

    # Set axis labels and a legend
    axe.set_xlabel('No of Iteration')
    axe.set_ylabel('Value')
    axe.legend()

    # Customize the appearance of the graph
    axe.grid(True)
    axe.set_title('Series Plot')
    
    # Display the plot or save it to a file
    if out_file:
        plt.savefig(out_file, dpi=300)
    else:
        plt.show()

if __name__ == '__main__':
    # Define command-line arguments
    parser = argparse.ArgumentParser(description='Plot series')
    parser.add_argument('-p', '--path', type=str, help='Path to the data file (csv, psv, or txt)', required=True)
    parser.add_argument('-o', '--outfile', type=str, default='', help='Path to save the output plot')
    
    # Parse command-line arguments
    args = parser.parse_args()
    file_path = args.path
    out_file = args.outfile

    # Read data from the specified file
    data = read_file(file_path)
    x = [row[0] for row in data]
    y = [row[1] for row in data]

    # Create and display the plot, optionally saving it to a file
    plot_results(x, y, out_file)
