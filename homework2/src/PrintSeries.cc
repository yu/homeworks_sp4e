#include "PrintSeries.hh"

// Constructor for the PrintSeries class, which takes a Series object, maximum iterations, and frequency as parameters

PrintSeries::PrintSeries(Series& series, unsigned int maxiter, unsigned int frequency) : DumperSeries(series) {

    this->frequency = frequency;        // Set the frequency for printing results

    this->maxiter = maxiter;            // Set the maximum number of iterations

}
// Method to dump (print) series-related information to an output stream
void PrintSeries::dump(std::ostream& os) {

    double rst1;
    double rst2;
    double convergence;

    int step_num = this->maxiter / this->frequency;

    for (int i = 1; i <= step_num; ++i) {

        rst1 = this->series.compute(i * this->frequency - 1);
        rst2 = this->series.compute(i * this->frequency);
        convergence = abs(this->series.getAnalyticPrediction() - rst2); // Calculate the convergence
        this->setPrecision(6); // Set the precision for output formatting

        // Output results in scientific notation with specified formatting
        os << std::scientific << std::setprecision(this->precision) << std::setw(5)
            << i * this->frequency << " " << std::setw(15)
            << rst1 << " " << std::setw(15)
            << rst2 - rst1 << " " << std::setw(15);
        if (!std::isnan(convergence)) {
            os << std::setw(15) << convergence; // If convergence is a valid number, output it
        }
        os << std::endl;

    }
}
