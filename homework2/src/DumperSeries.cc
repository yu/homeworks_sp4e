#include "DumperSeries.hh"

// Constructor for DumperSeries class
DumperSeries::DumperSeries(Series & series) : series(series) {
    // Initialize the precision to a default value of 6
    this->precision = 6;

}


void DumperSeries::setPrecision(unsigned int precision) {
// Update the precision member variable with the provided value
    this->precision = precision;

}
