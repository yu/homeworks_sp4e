//
// Created by Lenovo on 11/1/2023.
//
#ifndef RIEMANNINTEGRAL_HH
#define RIEMANNINTEGRAL_HH

#include <functional>

#include "Series.hh"

class RiemannIntegral: public Series {

public:
    RiemannIntegral(double a, double b, std::function<double(double)> f);
    double compute(unsigned int c) override;
    double computeTerm(unsigned int k) override;

private:
    double a;
    double b;
    double dx;
    std::function<double(double)> f;

};

#endif //RIEMANNINTEGRAL_HH


double x3(double x);
double cosx(double x);
double sinx(double x);