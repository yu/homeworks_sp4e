//
// Created by mshah on 10/31/2023.
//
#include "Series.hh"
Series::Series(){
    // Constructor initializes the object with default values.
    this->current_index = 0; // Initialize current index to 0.
    this->current_value = 0; // Initialize current value to 0.
    this->use_previous = true; // Initialize use_previous flag to true.
}

double Series::compute(unsigned int N) {
    if (this->current_index <= N && this->use_previous){
        // If the current index is less than or equal to the requested number of iterations in the series
        // and use_previous flag is true, we can reuse the previous value and iterate up to N - current_index.
        N -= this->current_index;
    }
    else{
        // If the current index is greater than N or use_previous flag is false,
        // reset the current_index and current_value to start fresh.
        this->current_index = 0;
        this->current_value = 0;
    }

    for (int k = 0; k < N; ++k) {
        // Iterate 'k' times to add terms to the series.
        this->addTerm();
    }
    // Return the computed series value.
    return this->current_value;
}
// Method to provide an analytic prediction, returns NaN by default
double Series::getAnalyticPrediction() {return nan("");}

// Method to add a term to the series
void Series::addTerm(){
    this->current_index += 1;
    this->current_value += this->computeTerm(this->current_index);

}
