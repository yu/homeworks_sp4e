# Homework 2
Group Members: Yuhan and Mati Ullah

## Requirement

### C++

* cmake

* g++

### Python

* Python 3.11

* Numpy 1.24.3

* Matplotlib 3.7.2

* Scipy 1.11.1


# How to execute the this program
This program can be execute both from command line using input arguments and integrated development enviroment(IDE). 

## Usage

To compile and build the programme, following the commands below:

```
cd homework2
mkdir build
cd build
ccmake ..
make
``` 


To run the code and do computation, arguments should be input, possible arguments includes:

`series type`: Series type of computation, possible arguments are `arithmetic`, `pi` and `integral`;

`output`: Output option, choose from print on screen `print` or write in a file `write`;

`iteration`: "Number of iteration, should input a int type number;

`frequency`: Record frequency, should input a int type number;

`format`: Format of output file, is valid when choose to write the results in the file, options are `txt`, `csv` and `psv`, default value is `txt`.

`a`: Lower limit a of the intergal, valid when choose `integral` in series type, should input a double type number.

`b`: Upper limit b of the intergal, valid when choose `integral` in series type, should input a double type number.

`function`: Argument to control the function type in the intergal calculation, possible choices are `x3`, `sin` and `cos`.

To run the code, a general format should be:

* Compute a series (input argument `arithmetic` or `pi` for series type):

```
./src/homework2 [series type] [output] [iteration] [frequency] [format]
```

* Calculate the integral (input argument `integral` for series type):

```
./src/homework2 [series type] [output] [iteration] [frequency] [a] [b] [function] [format]
```

Thus, 5 or 8 arguments should be input. A Detect procedure will be executed to confirm that the number of arguments entered meets the requirements.

Some examples are showing below:

```
./src/homework2 arithmetic print 1000 50 NA
```
`NA` means the output file format is not valid here, anything can be input here in this case.

For this command, results will be displayed on the screen:

```
INPUT ARGUMENTS CONFIRM
Series type: arithmetic
Output option: print
Number of iteration: 1000
Record frequency: 50
File format: NA
   50    1.225000e+03    5.000000e+01    0.000000e+00
  100    4.950000e+03    1.000000e+02    0.000000e+00
  150    1.117500e+04    1.500000e+02    0.000000e+00
  200    1.990000e+04    2.000000e+02    0.000000e+00
  250    3.112500e+04    2.500000e+02    0.000000e+00
  300    4.485000e+04    3.000000e+02    0.000000e+00
  350    6.107500e+04    3.500000e+02    0.000000e+00
  400    7.980000e+04    4.000000e+02    0.000000e+00
  450    1.010250e+05    4.500000e+02    0.000000e+00
  500    1.247500e+05    5.000000e+02    0.000000e+00
  550    1.509750e+05    5.500000e+02    0.000000e+00
  600    1.797000e+05    6.000000e+02    0.000000e+00
  650    2.109250e+05    6.500000e+02    0.000000e+00
  700    2.446500e+05    7.000000e+02    0.000000e+00
  750    2.808750e+05    7.500000e+02    0.000000e+00
  800    3.196000e+05    8.000000e+02    0.000000e+00
  850    3.608250e+05    8.500000e+02    0.000000e+00
```

An example to write the results in to a file:
```
./src/homework2 pi write 1000 100 csv
```

With this command, the computing results would be stored in a csv file in path `/build/src`.

An example to calculate the integral:

```
./src/homework2 integral print 1000 50 0 3.1415926 sin NA
```

## Plot Series
plot.py is a Python script that reads numerical data from a file and creates a line plot to visualize the data. It automatically detects the separator in the file and allows you to save the plot as an image.To create plot using this file, use the python enviroment and input arguments should be provided from command line in the given way:
```
cd homework2
cd src
python plot.py -p "plot.py -p "C:\Users\Lenovo\homework2\numerical_results.txt"

```
The above provided path is according to the developer system. The accurate path should be provided after clone this work.

# Answers to questions:
## Excercise-02
1. What is the best way/strategy to divide the work among yourselves? 

Answer:
* We split tasks - one person worked on declaring methods and making header files, and the other person worked on implementing methods.
* We both took care of method declaration and implementation, making it easier to work together and get the job done faster.
* We divided the work into parts, so one of us handled the beginning of methods, while the other completed the rest, making teamwork more efficient.

## Excercise-05
1. Evaluate the global complexity of your program.

Answer: The global complexity of the program is O(n!)

4. How is the complexity now ?

Answer: After refactoring the code, the complexity reduced to O(n)

5. In the case you want to reduce the rounding errors over floating point operation by summing terms
reversely, what is the best complexity you can achieve ?

Answer: In this way the complexity would be O(n^2)

## Excercise-06
4. Do you get the values expected ? For each function, at which value of N do you get the value expected
(∼ 2 digits after the decimal point)?

Answer: Expected integral values are:
*  Integral1 = 0.25
*  Integral2 = 0
*  Integral3 = 1

To achieve around 2 decimal places of accuracy, use the following N values:
* For integral1, use N = 102 to get integral1 ≈ 0.254975.
* For integral2, use N = 1 to get integral2 ≈ 0.
* For integral3, use N = 158 to get integral3 ≈ 1.0049940.
