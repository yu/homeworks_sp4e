import numpy as np
import scipy as sp
from GMRES import gmres
from matplotlib import cm
import argparse
import matplotlib.pyplot as plt


# Create an argument parser
parser = argparse.ArgumentParser(description="Optimizstion of a quadratic function using methods(bfgs, lgmres, gmres).")

# Add an argument for method selection
parser.add_argument("--method", choices=["bfgs", "lgmres", "gmres"], default="gmres",
                    help="Select the optimization methods (bfgs, lgmres, gmres). By default the selected optimization is gmres.")

# Add an argument to disable plotting by default
parser.add_argument("--no-plot", dest="plot", action="store_false",
                    help="Disable plotting of the optimization function. By default, plotting is turned on.")

# Add arguments for custom values of A and b
parser.add_argument("--A", type=str, default="8 1 1 3",
                    help="Enter the matrix A values in this way, '1 2 3 4'. By default selected values are: '8 1 1 3'.")
parser.add_argument("--b", type=str, default="2 4",
                    help="Enter the vector b values in this way, '1 2'. By default selected values are '2 4'.")

# Parse the command-line arguments
args = parser.parse_args()

# Extract custom values of A and b from the command-line arguments
A_values = list(map(float, args.A.split()))
b_values = list(map(float, args.b.split()))

# Create matrix A and vector b using custom values
A = np.array(A_values).reshape((2, 2))
b = np.array(b_values)

# Define initial guess and the quadratic function using a lambda function approach
X0 = np.array([0, 0])
S = lambda X: 0.5 * np.dot(X, np.dot(A, X.T)) - np.dot(X, b)

# Define an empty list for each iteration results recording
optimization_record = [X0]

# Define a callback function to store each iteration result
def callback(X):
    optimization_record.append(X)

if args.method == "bfgs":
    # Optimization method-01,bfgs by Scipy
    result = sp.optimize.minimize(S, X0, callback=callback)
    x = result.x
    optimization_record = np.array(optimization_record)
elif args.method == "lgmres":
    # Optimization method-02,lgmres by Scipy
    result = sp.sparse.linalg.lgmres(A, b, callback=callback)
    x = result[0]
    optimization_record = np.array(optimization_record)
elif args.method == "gmres":
    # Optimization method-03, gmres, this function is defined by us.
    x, e, x_hist = gmres(A, b, X0, callback=None, tol=1e-7)
    optimization_record = np.array(x_hist)

# Print the optimization result
print(f'Selected method ({args.method}), Optimization result: {x}')
print("Value at iteration step: ", optimization_record)

# Plotting function (if plot by default)
if args.plot:
    X = np.arange(-3, 3, 0.1)
    Y = np.arange(-3, 3, 0.1)
    X, Y = np.meshgrid(X, Y)
    Z = 0.5 * (A[0, 0] * X**2 + (A[0, 1] + A[1, 0]) * X * Y + A[1, 1] * Y**2) - b[0] * X - b[1] * Y

    figure = plt.figure()
    ax = figure.add_subplot(111, projection='3d')

    # Plot the surface with a lighter colormap
    ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, alpha=0.35)

    # Plot the optimization path
    ax.plot(optimization_record[:, 0], optimization_record[:, 1], [S(X) for X in optimization_record], marker='o', color='r', linestyle='--', label=(args.method))
    contours = ax.contour(X, Y, Z, 10, colors='k', linestyles='solid')

    # Set the figure title
    ax.set_title(args.method.upper())

    # Set the axes
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('S(x)')
    ax.invert_xaxis()
    ax.invert_yaxis()
    ax.set_zlim([0, 75])

    # Set the view
    ax.view_init(elev=55, azim=-40)

    plt.show()
