import sys
import numpy as np
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres
import matplotlib.pyplot as plt

# Create the quadratic function S(x)
def fun(A, b):
    S = lambda x: 0.5 * np.dot(x.T, np.dot(A, x)) - np.dot(x.T, b)
    return S

# Function of the minimzer tools
def minimizer(A, b, x0, method):
    
    print("The solver you choose:", method)

    istep = [x0]        # to store the value of the iteration steps

    # to record the results of each iteration step
    def cb(x):
        istep.append(x)
        return
    
    if method == "BFGS":
        rst = minimize(fun(A, b), x0, method="BFGS", callback=cb)
        print("Minimizer result:", rst.get("x"))
        print("Value at iteration step: ", np.array(istep))

    elif method == "LGMRES":
        rst = lgmres(A, b, x0, atol=1e-05, callback=cb)
        print("Minimizer:", rst[0])
        print("Value at iteration step: ", np.array(istep))

    else:
        print("Method undefined, please check!")
    
    istep3d = np.array([np.array([coor_xy[0], coor_xy[1], fun(A, b)(coor_xy)]) for coor_xy in istep])

    return istep3d

# Figure plotting
def fig_plot(istep3d, fig_title, A, b):
    # plot mesh define
    X = np.arange(-3, 3, 0.05)
    Y = np.arange(-3, 3, 0.05)
    coor_X, coor_Y = np.meshgrid(X, Y)
    coor_Z = np.array([[fun(A, b)(np.array([x, y])) for x in X] for y in Y])

    # plot figure
    fig1 = plt.figure()
    ax1 = plt.axes(projection='3d')

    ax1.plot_surface(coor_X, coor_Y, coor_Z,cmap='coolwarm', edgecolor='none', alpha=0.35)
    ax1.contour3D(coor_X, coor_Y, coor_Z, 10)

    ax1.plot(istep3d[:, 0], istep3d[:, 1], istep3d[:, 2], "r--", linewidth=1)
    ax1.scatter(istep3d[:, 0], istep3d[:, 1], istep3d[:, 2], linewidth=1, color="red")

    # Set the title, axes
    ax1.set_title(fig_title)
    ax1.set_xlabel("x")
    ax1.set_ylabel("y")
    ax1.invert_xaxis()
    ax1.invert_yaxis()

    ax1.set_zlim([0, 75])

    ax1.view_init(elev=55, azim=-40)

    plt.show()

    return


def main(method=1):

    # set the name of method
    if method == 1:
        method = "BFGS"
    elif method == 2:
        method = "LGMRES"
    else:
        print("Method undefined, please check!")
        sys.exit(1)
    
    A = np.array([[8, 1], [1, 3]])
    b = np.array([2, 4])
    x0 = np.array([0, 0])

    iteration_rec = minimizer(A, b, x0, method)
    fig_plot(iteration_rec, method, A, b)

    return


if __name__ == "__main__":
    main(1)
    main(2)