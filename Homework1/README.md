# Homework 1

## Requirements

### Environment

* Python 3.11

### Package version

* Numpy 1.24.3

* Matplotlib 3.7.2

* Scipy 1.11.1

## Exercise 1: *Scipy optimization*

### Code execution

To execute the code, go the folder of homework 1 `homeworks_sp4e\Homework1`, and run file `optimizer.py`:

```
python optimizer.py
```

### Output

The given minimization problem would be solve by `scipy.optimize.minimize` and `scipy.sparse.linalg.lgmres` routines separately. The results and the values at iteration step would be printed on the screen.

```
The solver you choose: BFGS
Minimizer result: [0.08695651 1.30434782]
Value at iteration step:  [[0.         0.        ]
 [0.45168572 0.90337147]
 [0.11333106 1.3615788 ]
 [0.08685195 1.30412093]
 [0.08695651 1.30434782]]
 ```
 ```
The solver you choose: LGMRES
Minimizer: [0.08695652 1.30434783]
Value at iteration step:  [[0.         0.        ]
 [0.08695652 1.30434783]
 [0.08695652 1.30434783]]
 ```

Figures showing the iteration steps with the two routines would also be plotted.

![BFGS](figures/E1-BFGS.png)
![LGMRES](figures/E1-LGMRES.png)

## Exercise 2: *Generalized minimal residual method*

### Code execution

Go the folder of homework 1 `homeworks_sp4e\Homework1`, file `GMRES.py` and `exercise2.py` can be found.

To execute the code, a possible command could be:

```
python exercise2.py --method "bfgs" --A "8 1 1 3" --b "2 4"
```

To control the plot option, `--no-plot` argument is used, with command like:

```
python exercise2.py --method "bfgs" --no-plot --A "8 1 1 3" --b "2 4"
```

Descriptions of the input arguments:

* `--method` Select the optimization method. Options are `"bfgs"`, `"lgmres"` and `"gmres"`. Of which the method option `"gmres"` is related to a self-defined method, `"bfgs"` and `"lgmres"` are the optimization method provide by Scipy. By default the selected optimization is `"gmres"`.

* `--no-plot` Plotting option. This argument is used to disable plotting of the optimization function. Plotting is turned on by default if not input this argument.

* `--A` Input the matrix *A*. The matrix should be enter like `"1 2 3 4"`. Default value `"8 1 1 3"`.

* `--b` Input the vector *b*. The vector should be enter like `"1 2"`. Default value `"2 4"`.

### Output

The optimization results and the values at iteration step will be printed on the screen.

Figure of the solving step would be plot if the plotting option is turned on. Some examples are shown here.

```
python exercise2.py --method "bfgs" --A "8 1 1 3" --b "2 4"
```

![2-1](figures/E2-1.png)

```
python exercise2.py --method "lgmres" --A "8 1 1 3" --b "2 4"
```

![2-2](figures/E2-2.png)

```
python exercise2.py --method "gmres" --A "8 1 1 3" --b "2 4"
```

![2-3](figures/E2-3.png)


```
python exercise2.py --method "bfgs" --A "8 3 2 3" --b "2 4"
```

![2-4](figures/E2-4.png)

```
python exercise2.py --method "lgmres" --A "8 1 3 2" --b "5 2"
```

![2-5](figures/E2-5.png)

```
python exercise2.py --method "gmres" --A "6 1 3 5" --b "5 1"
```

![2-6](figures/E2-6.png)


