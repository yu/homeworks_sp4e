import numpy as np

#This code is developed according to the https://en.wikipedia.org/wiki/Generalized_minimal_residual_method
#########################################################################################
#                            Defining the GRMES Function                                #
#########################################################################################

def gmres(A, b, x0, max_iteration=50, tol=1e-17, callback=None):
    m = max_iteration
    x_hist=[]
    # use x as the initial vector
    r = b - np.einsum('ij,j->i', A, x0)

    b_norm = norm(b)
    error = norm(r) / b_norm

    # initialize the 1D vectors
    sn = np.zeros(m)
    cs = np.zeros(m)
    e1 = np.zeros(m+1)

    e1[0] = 1
    e = [error]
    r_norm = norm(r)
    Q = np.zeros((len(r), m + 1))
    H = np.zeros((len(r) + 1, m))
    Q[:, 0] = r / r_norm
    beta = r_norm * e1
    for k in range(m):

        # Run arnoldi
        H[:k+2, k], Q[:, k+1] = arnoldi(A, Q, k)

        # Eliminate the last element in H ith row and update the rotation matrix
        H[:k+2, k], cs[k], sn[k] = apply_givens_rotation(H[:k+2, k], cs, sn, k)

        # update the residual vector
        beta[k+1] = -sn[k] * beta[k]
        beta[k] = cs[k] * beta[k]
        error = np.abs(beta[k+1]) / b_norm
        y = np.einsum('ij,j->i', np.linalg.inv(H[:k+1, :k+1]), (beta[:k+1]))
        x = x0 + np.einsum('ij,j->i', Q[:, :k+1],  y)
        x_hist.append(x)
        # save the error
        e.append(error)

        if error <= tol:
            break

    if k == m-1:
        print("Not coverged")
    # calculate the result
    y = np.einsum('ij,j->i', np.linalg.inv(H[:k+1, :k+1]), (beta[:k+1]))
    x = x0 + np.einsum('ij,j->i', Q[:, :k+1],  y)
    

    if callback is not None:
        callback(x)

    return x, e,x_hist


#####################################################################################
#                                Arnoldi Function                                   #
#####################################################################################
def arnoldi(A, Q, k):
    q = np.einsum('ij,j->i', A, Q[:, k])  # Krylov Vector
    h = np.zeros(k + 2)
    for i in range(k+1):  # Modified Gram-Schmidt, keeping the Hessenberg matrix
        h[i] = np.einsum('i,i', q, Q[:, i])
        q -= h[i] * Q[:, i]
    h[k+1] = norm(q)
    q = q / h[k+1]
    return h, q


#####################################################################################
#                          Applying Given Rotation to H col                         #
#####################################################################################
def apply_givens_rotation(h, cs, sn, k):
    # apply for ith column
    for i in range(k):
        temp = cs[i] * h[i] + sn[i] * h[i + 1]
        h[i+1] = -sn[i] * h[i] + cs[i] * h[i + 1]
        h[i] = temp

    # update the next sin cos values for rotation
    cs_k, sn_k = givens_rotation(h[k], h[k+1])

    # eliminate H(i + 1, i)
    h[k] = cs_k * h[k] + sn_k * h[k+1]
    h[k+1] = 0.0
    return h, cs_k, sn_k


#####################################################################################
#                          Calculate the Given rotation matrix                      #
#####################################################################################
def givens_rotation(v1, v2):
    t = (v1**2 + v2**2)**0.5
    cs = v1 / t
    sn = v2 / t
    return cs, sn


#####################################################################################
#                                   Calculate the Norm                              #
#####################################################################################
def norm(x):
    return np.sqrt(np.einsum('i,i', x, x))

