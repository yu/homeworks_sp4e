#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "compute_temperature.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  // bind the routines here

  // Create python bindings for factory interface classes
  //Particles Factory
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
          .def("createSimulation",
               py::overload_cast<const std::string&, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>),
               py::return_value_policy::reference)
          .def("createSimulation",
                [](ParticlesFactoryInterface& self, const std::string& fname, Real timestep) -> SystemEvolution& {
                    return self.createSimulation(fname, timestep);},
                py::return_value_policy::reference)
          .def("getInstance", &ParticlesFactoryInterface::getInstance,
               py::return_value_policy::reference)
          .def_property_readonly("system_evolution",
                                 &ParticlesFactoryInterface::getSystemEvolution);

  //Material Points Factory
  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
          .def("getInstance", &MaterialPointsFactory::getInstance,
               py::return_value_policy::reference);

  //Planets Factory
  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
          .def("getInstance", &PlanetsFactory::getInstance,
               py::return_value_policy::reference);

  //Ping Pong Balls Factory
  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
          .def("getInstance", &PingPongBallsFactory::getInstance,
               py::return_value_policy::reference)
          .def("createParticle", &PingPongBallsFactory::createParticle,
             py::return_value_policy::reference);

  // Create python binding for classes Compute
  //Compute
  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");

  //Compute Interaction
  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction");

  //Compute Gravity
  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
            .def(py::init<>())
            .def("setG", &ComputeGravity::setG, py::arg("G") = 1.);

  //Compute Temperature
  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
        .def(py::init<>())
        .def_property(
                "conductivity", &ComputeTemperature::getConductivity,
                [](ComputeTemperature& c, Real val) { c.getConductivity() = val; })
        .def_property("L", &ComputeTemperature::getL,
                      [](ComputeTemperature& c, Real val) { c.getL() = val; })
        .def_property("capacity", &ComputeTemperature::getCapacity,
                [](ComputeTemperature& c, Real val) { c.getCapacity() = val; })
        .def_property("density", &ComputeTemperature::getDensity,
                [](ComputeTemperature& c, Real val) { c.getDensity() = val; })
        .def_property("deltat", &ComputeTemperature::getDeltat,
                [](ComputeTemperature& c, Real val) { c.getDeltat() = val; });

  //Compute Verlet Integration
  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
          .def(py::init<Real>())
          .def("addInteraction", &ComputeVerletIntegration::addInteraction)
          .def("deltat", &ComputeVerletIntegration::setDeltaT)
          .def("compute",&ComputeVerletIntegration::compute);


  // Create python binding for other necessary classes
  //CSV Writer
  py::class_<CsvWriter>(m, "CsvWriter")
        .def(py::init<const std::string&>())
        .def("write", &CsvWriter::write);

  //System Evolution
  py::class_<SystemEvolution>(m, "SystemEvolution")
        .def("getSystem", &SystemEvolution::getSystem)
        .def("setNSteps", &SystemEvolution::setNSteps)
        .def("setDumpFreq", &SystemEvolution::setDumpFreq)
        .def("evolve", &SystemEvolution::evolve)
        .def("addCompute",&SystemEvolution::addCompute);

  //System
  py::class_<System>(m, "System", py::dynamic_attr())
        .def(py::init())
        .def("getNbParticles" , &System::getNbParticles)
        .def("getParticle", &System::getParticle);


}
