import numpy as np
import argparse
import csv
import os 

def main():
    '''
        Function to calculate the trajectories error of the simulation for a given planet 
        given trajectories of reference 

    Returns
    -------
        None. Prints on screen the calculated error. 

    '''
    #parse arguments 
    parser = argparse.ArgumentParser(
        description='Compute simulation error for given planet')
    parser.add_argument('-p', '--planet', type=str,
                        help=('Name of planet. E.g. [--planet mercury]'),
                        required=True)
    parser.add_argument('-dir', '--directory', type=str,
                        help=('Directory where trajectory files (to test) are saved, e.g. [ --directory ./dumps/]'),
                        required=True)
    parser.add_argument('-dir_r', '--directory_ref', type=str,
                        help=('Directory where reference trajectory files are saved, e.g. [ --directory_ref ./trajectories/]'),
                        required=True)
    
    #argument parser 
    args = parser.parse_args()
    planet = args.planet
    directory = args.directory
    directory_ref = args.directory_ref
    
    #Read trajectories to test 
    positions = readPositions(planet, directory)
    
    #Read reference trajectories 
    positions_ref = readPositions(planet, directory_ref)  
    
    #Compute the integral error between the two 
    integ_error = computeError(positions, positions_ref)  
    
    #print result on screen 
    print("The integral error of the position of planet <<" + str(planet) +">> in [X, Y, Z] dimensions is " + str(integ_error))

def readPositions(planet_name, directory):
    ''' 
        Function to read the trajectory of a given planet from given directory and
        store to a numpy array [m x n], m = 365 days, n=3 trajectory components 
        
        Parameters: 
        -----------    
        planet_name:  Name of the planet to read the trajectory in the file 
        directory  :  The directory where trajectory files are stored
        
        
        Returns: 
        -------- 
        coordinates: numpy array (365 x 3) of X-Y-Z trajectories per day
        
    '''
    # Get the list of all files and directories 
    # in the root directory 
    
    dir_list = os.listdir(directory) 
    dir_len = len(dir_list)
    
    
    #initialize np array
    coordinates = np.zeros((365,3))
    counter = 0
    #scan all files in the directory 
    for i in range(dir_len):
        
        
        #Open and read csv files from directory
        filename = directory + dir_list[i]
        
        #make sure to only read trajectory files with names starting by 'step-'
        if (filename[len(directory):len(directory)+5]=='step-'):
            if counter>364 :
                print('Warning: More than 365 files in directory! Some files might not be read!')        
                break
            counter += 1  
            #need to recognise name format (no of digits) - number of preceding zeros may differ 
            #assign array index according to file name 
            
            j = int(filename[len(directory)+5:-4])
            if j>364:
                print('Warning: file_id >= 365 - file will not be read')
            else:        
                with open(filename, 'r') as csv_file:
                    reader = csv.reader(csv_file, delimiter = ',')
                    for row in reader:
                        if (len(row)>0):   #check to ignore empty lines
                            line = row[0].split()
                            if line[-1] == planet_name:
                                
                                #read coordinates and store to array
                                for k in range(3):
                                    coordinates[j,k] = float(line[k])
                               
                                #exit file reading loop  
                                break 
       

    #save coordinates to file 
    f = open("read_coord_" + str(directory[-6:-2]), 'w')
    np.savetxt(f, coordinates)
    f.close()
    return coordinates    


def computeError(positions, positions_ref):
    '''
        Function to compute the integral error between two trajectories 
        (the second input is the reference)


    Parameters
    ----------
    positions : numpy array of trajectories to test.
    positions_ref : numpy array or reference trajectories.

    Returns
    -------
    E: the integral error in 3 dimensions [X,Y,Z] as numpy.array [1 x 3] 

    '''
    E = np.sqrt(np.sum(np.power(np.absolute(positions_ref - positions),2),axis=0))

    
    return E
    
    if __name__ == "__main__":
    main()
