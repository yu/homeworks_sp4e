from scipy.optimize import fmin
import generate_input_launch as gn
import numpy as np 
import matplotlib.pyplot as plt
import argparse

def main():
    '''
        Main function to execute the optimization routine and plot the results

    '''

    #parse arguments 
    parser = argparse.ArgumentParser(
        description='Find optimal scale factor to correct velocity of a planet for minimizing trajectory integral error')
    parser.add_argument('-p', '--planet', type=str,
                        help = ('Name of planet. E.g. [--planet mercury]'),
                        required=True)
    parser.add_argument('-dir', '--directory', type=str,
                        help = ('Directory where trajectory files (to test) are saved, e.g. [ --directory ./dumps/]'),
                        required=True)
    parser.add_argument('-dir_r', '--directory_ref', type=str,
                        help = ('Directory where reference trajectory files are saved, e.g. [ --directory_ref ./trajectories/]'),
                        required=True)
    parser.add_argument('-f','--filename', type=str, required=False, default='init.csv',
                        help = 'Name of input file, e.g. [--filename init.csv]') 
    parser.add_argument('-s', '--scale', type=float, default=1.0, required=False,
                        help = 'Value of scaling factor (OPTIONAL - default = 1.0), e.g. [--scale 1]')    
    parser.add_argument('-s_as','--save_plot_as', required=False, default='', 
                        help = 'filename to save figure (OPTIONAL) e.g. [--save_plot_as my_plot]') 
    parser.add_argument('--acc_tol', type=float, required=False, default=1e-6, 
                        help = ' accepted tolerance for scaling factor, e.g. [--acc_tol 1e-6]') 

    args = parser.parse_args()

    scale_guess = args.scale 
    planet_name = args.planet
    input_file = args.filename
    directory = args.directory
    directory_ref = args.directory_ref
    file_plot = args.save_plot_as
    acc_tol = args.acc_tol
    
    freq = 1     
    nb_steps = 365 
    
    #call optimizer function  
    res, iter_res = scipyVelocityOptimizer(scale_guess, planet_name, nb_steps, input_file, freq, directory, directory_ref, acc_tol)
    
    #Print out the optimized result
    print('Optimal scale value is ' + str(res[0]))
    
    
    #Create plot of error vs. scaling factor for every step of the optimization process
    plotOptimizationResults(iter_res[0], iter_res[1], file_plot)
    
    #Print the corresponding velocities 
    
    return 

def scipyVelocityOptimizer(scale_guess, planet_name, nb_steps, input_file, freq, directory, directory_ref,acc_tol):
    '''
    Function to optimize velocity 

    Returns
    -------
    scale: Optimal scale factor to correct speeds for minimum error. 

    '''
    def storeIterationValues(scale):
        '''
            Function to store values (score,error) of every iteration 

        '''
        error = gn.runAndComputeError(scale, planet_name, input_file, nb_steps, freq,directory,directory_ref)
        iteration_res_scale.append(scale[-1])
        iteration_res_error.append(error)
        
        return
    
    # Arrange input as an array
    scale = np.array([scale_guess])
    
    # Create a list to store iteration results
    iteration_res_scale = [scale[0]]
    iteration_res_error = [gn.runAndComputeError(scale, planet_name, input_file, nb_steps, freq,directory,directory_ref)]
    
    print('Optimization starting...')
    
    res = fmin(gn.runAndComputeError, x0=scale, args=(planet_name, input_file, nb_steps, freq, directory, directory_ref), full_output=1, callback=storeIterationValues ,disp=1, retall=1, xtol=acc_tol)    
    
    # Pack the iteration results
    iter_res = [iteration_res_scale, iteration_res_error]

    return res, iter_res

def plotOptimizationResults(scale,error,outfile):
    
    fig = plt.figure()
    axe = fig.add_subplot(1, 1, 1)
    scale = np.array(scale)
    error = np.array(error)
    axe.plot(scale, error, marker='o')
    
    axe.set_xlabel(r'scale factor')
    axe.set_ylabel(r'integral  error')

    #save plot
    if outfile != "":
        plt.savefig(outfile)
        
    plt.show()  
    return
     
if __name__ == "__main__":
    main()    
