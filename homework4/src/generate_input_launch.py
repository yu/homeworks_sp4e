import numpy as np
import argparse
import csv
import sys
from main import main as launcher 
from simulation_error import readPositions, computeError

def main():
    ''' 
        Main function to compute simulation error for specific scaling factor applied 
        to initial velocity of a planet 
        (test for runAndComputeError function) 
    '''
    parser = argparse.ArgumentParser(description='Compute simulation error for specific scaling factor applied to initial velocity of a planet')
    parser.add_argument('-p', '--planet', type=str,
                        help = ('Name of planet. E.g. [--planet mercury]'),
                        required=True)
    parser.add_argument('-dir', '--directory', type=str,
                        help = ('Directory where trajectory files (to test) are saved, e.g. [ --directory ./dumps/]'),
                        required=True)
    parser.add_argument('-dir_r', '--directory_ref', type=str,
                        help = ('Directory where reference trajectory files are saved, e.g. [ --directory_ref ./trajectories/]'),
                        required=True)
    parser.add_argument('-f','--filename', type=str, required=False, default='init.csv',
                        help = 'Name of input file, e.g. [--filename init.csv]') 
    parser.add_argument('-s', '--scale', type=float, default=1.0, required=False,
                        help = 'Value of scaling factor (OPTIONAL - default = 1.0), e.g. [--scale 1]')    
    

    args = parser.parse_args()
    scale = np.array([args.scale]) 
    planet_name = args.planet
    input_file = args.filename
    directory = args.directory
    directory_ref = args.directory_ref
    
    nb_steps = 365 
    freq = 1     
    
    #Get the sum of the integral error in 3 dimensions 
    integ_error = runAndComputeError(scale, planet_name, input_file, nb_steps, freq, directory, directory_ref)
    
    print('The integral error of ' + planet_name + ' for scale ' + str(scale) + ' is ' + str(integ_error))
    
def generateInput(scale, planet_name, input_filename, output_filename):
    '''
    Function to generate input file from a given input file by scaling 
    velocity of a given planet. 

    Parameters
    ----------
    scale : scaling factor for velocity.
    planet_name : name of the planet to scale speed.
    input_filename :  name of the initial input .csv file; if it is in a different 
                    directory, give as full path name 
    output_filename : name of the generated input file with scaled speed.

    Returns
    -------
    None.

    ''' 
    scale = scale[0] #if input is np.array as the optimizer does, we have to transform it to a simple number 
    
    if (input_filename[-3:]!='csv'):
        sys.exit('Input file must be of .csv type.')
    
    if (output_filename[-4:]!='.csv'):
        output_filename += '.csv' 
        
    #open the new output file to write 
    with open(output_filename,'w') as csv_out_file: 
        writer = csv.writer(csv_out_file, delimiter = ' ')
        
        #open and read initial input file 
        with open(input_filename, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter = ' ')
            for row in reader:
                #remove last empty element of row caused by delimiter being ' '
                if not row[-1]: 
                    del row[-1]
                if (row[-1] == planet_name):
     
                    #scale speed coordinates
                    for k in range(3,6):
                        row[k] = str(scale*float(row[k]))
                                           
                
                #write line to output file 
                writer.writerow(row)
    return 

def launchParticles(input_file, nb_steps, freq) :
    '''
        Function to launch particle code for planets on a provided input file
        with time step of 1 day
    '''
    
    launcher(nb_steps,freq,input_file,'planet',1)
    
    return 

def runAndComputeError(scale,planet_name,input_file,nb_steps,freq,directory,directory_ref):
    '''
        Function to run and compute the error of the simulation for a given planet and 
        scaling velocity factor. 
    

    Parameters
    ----------
    scale : scaling factor for velocity.
    planet_name : name of the planet to scale speed..
    input_file : name of the input .csv file to launch the simulation; if it is in a different 
                    directory, give as full path name
    nb_steps : number of simulation steps to perform.
    freq : frequency of printing trajectories in dump files.

    Returns
    -------
    integ_error: the error of the simulation in calculating position in [X,Y,Z] 
    dimensions based to the refernce file.

    '''
    #Step 1 - generate new input file with scaled velocity
    
    new_filename = 'scaled_init.csv' 
    
    generateInput(scale, planet_name, input_file, new_filename)
    
    #Step 2 - launch simulation with the new input file 
    
    launchParticles(new_filename, nb_steps, freq)
    
    #Step 3 - compare output files and calculate simulation error (these must be inputs probably)
    
    #gather all position coordinates of the planer in one array
    positions = readPositions(planet_name, directory)
    positions_ref = readPositions(planet_name, directory_ref)  
    
    integ_error = computeError(positions, positions_ref)
    aggregated_error = np.sum(integ_error)
    
    return aggregated_error

    if __name__ == "__main__":
    main() 
