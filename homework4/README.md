# Homework 4

Authors: Yuhan & Mati Ullah

## Requirement

Minimum requirments of C++ environment are given below:

* cmake

* g++

* FFTW

* GoogleTest

* eigen

* pybind11

Minimum requirments of C++ environment are given below:

* numpy

* matplotlib

* scipy

* csv

* argparse

## Usage

To compile and build the programme, following the commands below:

```
cd homework4/src
mkdir build
cd build
ccmake ..
make
``` 


## Answer to Questions

#### Exercise 1

2. In class `ParticlesFactory`, `createSimulation` function has been overloaded to take functor as one of its argument. Comment on what this function is doing?

**Answer**: The `createSimulation` function is overloaded to allow it to accept a functor as one of its arguments. This allows users to pass extra parameters to the compute object and modify how the system evolution is computed. The user can choose different compute types, such as `ComputeTemperature`, which sets the conductivity and heat capacity. The compute object will use default values if the user does not provide a custom function.

#### Exercise 2

2. How will you ensure that references to Compute objects type are correctly managed in the python bindings?

**Answer**: In the `SystemEvolution` class, the `addCompute` function accepts an argument of the Compute type in a shared pointer. It is necessary to define a type to manage the object reference. Here we introduce `Compute` object, denoted as `std::shared_ptr<...>`, in the python binding. According to the documentation of Pybind11, smart pointers can be taken an alternative method.

#### Exercise 5
In this exercise, we made two functions and saved them in a file called `simulation_error.py`. To check if they work well, we created a main function in the same file.This main function is designed to compute the trajectory error for a particular planet based on trajectory files generated. The calculated error is then compared against a predefined set of reference trajectories, which are considered accurate.

#### Execution of 'simulation_error.py'
``` 
$ python3 simulation_error.py --planet [planet_name]  --directory [directory_of_traj_to_test] --directory_ref [directory_of_reference_traj]
``` 
#### Argument Specification:

* --planet: Specifies the name of the planet for which trajectory error is to be calculated.

* --directory: Specifies the directory where the generated trajectory files are stored. For example, [--directory ./dumps/].

* --directory_ref: Specifies the directory where the reference trajectory files are stored. For example, [--directory_ref ./trajectories/].

#### Example Usage:

To calculate the integral error of Mercury from trajectory files stored in ./dumps/ compared to reference trajectory files stored in ./trajectories/, the following command should be used:
```
$ python3 simulation_error.py --planet mercury --directory ./dumps/ --directory_ref ./trajectories/
```

#### Exercise 6
In this exercise, we made three functions and saved them in a file named `generate_input_launch.py`. To check the operation of these functions a main function is created. By running the program, it will calculate the integral error of a simulation. This is done by adjusting how fast a planet starts moving using a specific scale factor.

Execution of 'generate_input_launch.py'
```
$ python3 generate_input_launch.py -p [planet_name] -dir [directory_of_traj_to_test] -dir_r [directory_of_reference_traj] -f [input_filename] -s [scaling_factor]

```
#### Argument Specification:

* -p/--planet: Specifies the name of the celestial body for which trajectory error is to be calculated.

* -dir/--directory: Indicates the directory path where the generated trajectory files are stored. Example: [-dir ./dumps/].

* -dir_r/--directory_ref: Specifies the directory path where the reference trajectory files are stored. Example: [-dir_r ./trajectories/].

* -f/--filename: Specifies the name of the input file. Example: [-f init.csv] (optional, default= init.csv).

* -s/--scale: Specifies the value of the scaling factor applied. Example: [-s 0.6] (optional, default= 1.0).

#### Example Usage:

To calculate the trajectory error for Mercury with a scaling factor of 0.8, the following command should be used:
```
$ python3 generate_input_launch.py -p mercury -dir ./dumps/ -dir_r ./trajectories/ -f init.csv -s 0.8
```

#### Exercise 7
The optimization is carried out by the main function in the file Optimizer.py. To initiate an optimization from the command line, the following syntax is required:
```
$ python3 optimizer.py -p [planet_name] -dir [directory_of_traj_to_test] -dir_r [directory_of_reference_traj] -f [input_filename] -s [scaling_factor] --acc_tol [acc_tolerance_scale] --save_plot_as [figure_filename]

```
#### Argument Specification:

* -p/--planet: Specifies the name of the planet for which the trajectory error is to be calculated.

* -dir/--directory: Indicates the directory path where the generated trajectory files are stored. Example: [-dir ./dumps/].

* -dir_r/--directory_ref: Specifies the directory path where the reference trajectory files are stored. Example: [-dir_r ./trajectories/].

* -f/--filename: Specifies the name of the input file. For example, [-f init.csv] (optional, default= init.csv).

* -s/--scale: Specifies the value of the scaling factor applied. For example, [-s 0.6] (optional, default= 1.0).

* --acc_tol: Specifies the accepted tolerance for the scale values. For example, [--acc_tol 1e-3] (optional, default= 1e-6).

* -s_as/--save_plot_as: Indicates the name for saving the plot into a file. For example, [-s_as my_figure] (optional, by default the plot is not saved).

#### Example Usage:

To optimize the scaling factor for the planet Mercury, starting from the value 0.8, with the input file 'init.csv', and a tolerance of 1e-4, the command should be:
```
$ python3 optimizer.py -p mercury -dir ./dumps/ -dir_r ./trajectories/ -f init.csv -s 0.8 --acc_tol 1e-6 -s_as my_figure.png
```

